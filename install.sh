sudo apt-get install supervisor -y
sudo apt-get install gunicorn -y
git pull
sudo git submodule update --init --recursive
sudo git submodule update --recursive --remote
cd /server/ahrensburg
go build
cd /Server/lernen
mdbook clean
mdbook build
cd /Server/programmiersprachen
mdbook clean
mdbook build
cd /Server/ahrensburgeu
mdbook clean
mdbook build
cd /Server
sudo cp -u ahrensburg.service /etc/systemd/system/ahrensburg.service
# sudo cp -u python.conf /etc/supervisor/conf.d/python.conf
# sudo cp -u node.conf /etc/supervisor/conf.d/node.conf
sudo supervisorctl reread
sudo service supervisor restart
sudo  systemctl enable ahrensburg.service
sudo systemctl start ahrensburg.service
# dotnet publish --configuration Release