* https://github.com/caddyserver/certmagic

# Webserver
```
sudo apt-get update && sudo apt-get upgrade
sudo apt-get install git-core -y
apt-get install  gnupg
sudo apt-get install openjdk-8-jdk
sudo apt-get install tomcat9



```


* [Nodejs](https://github.com/nodesource/distributions/blob/master/README.md#debmanual)


## Caddy Installieren

* [Downloads](https://caddyserver.com/docs/install#debian-ubuntu-raspbian)


## Go Installieren

```

wget https://golang.org/dl/go1.16.2.linux-amd64.tar.gz
rm -R /usr/local/go
sudo tar -C /usr/local -xzf  go1.16.2.linux-amd64.tar.gz

```
## Mysql

* [Download Adresse](https://dev.mysql.com/downloads/)

```
wget -c https://dev.mysql.com/get/mysql-apt-config_0.8.16-1_all.deb
sudo dpkg -i mysql-apt-config*
sudo apt update
sudo apt-get install mysql-server
mysql -u username -p
$mysql create database  Test;
$mysql create database test;


```
## PHP
```
sudo apt -y install php-fpm php-mysql php-cli php-zip php-mbstring php-xml
```

## Python Installieren

``` 

sudo apt install python3 python3-dev git curl python-is-python3  python3-pip 
pip3 install ipython
pip3 install -U jupyter
jupyter
pip3 install -U  jupyterlab
jupyter-lab
pip3 install -U  notebook
jupyter notebook
pip3 install -U voila

```

## bash.bashrc

```

sudo nano /etc/bash.bashrc
----Datei----


export GOROOT=/usr/local/go
export GOPATH=/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH


---Datei ende---
source  /etc/bash.bashrc

```

## C und C++

```
sudo apt update
sudo apt upgrade
sudo apt-get install cmake gcc clang gdb build-essential git-core

```
## Rust Installieren

* [Rust Downloads](https://www.rust-lang.org/tools/install)

## mdbook Installieren

```
cargo install mdbook

```

## Server Installieren

```
git clone https://github.com/thorstenkloehn/Server.git /Server


```

## Apache Zeppelin

```

sudo apt-get install libfontconfig
sudo apt-get install r-base-dev
sudo apt-get install r-cran-evaluate
pip3 install jupyter
pip3 install grpcio
pip3 install protobuf

mkdir /zeppelin
wget https://downloads.apache.org/zeppelin/zeppelin-0.9.0/zeppelin-0.9.0-bin-all.tgz
sudo tar -C /zeppelin -xzf zeppelin-0.9.0-bin-all.tgz
cd /zeppelin/zeppelin-0.9.0-bin-all
cp conf/shiro.ini.template conf/shiro.ini
cd conf
nano shiro.ini
cd ..
bin/zeppelin-daemon.sh start

```

## Aktualisieren

Um Submodule zu aktualisieren, können wir verwenden

```
git submodule update --recursive --remote
```

oder

```

git pull --recurse-submodules


```

## Submodule hinzufügen

```

git submodule add https://github.com/thorstenkloehn/webassemblygo tinygo


```

## Submodule löschen

```

git submodule deinit submodule_name
git rm submodule_name


```
